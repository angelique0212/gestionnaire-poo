<?php


class Voiture{
    public $couleur;
    private $nbRoue;
    public $anneeProduction;


    public function __construct($anneeProduction)
    {
        $this ->anneeProduction =$anneeProduction;
    }


    public function __toString()
    {
        return $this->couleur . ' (' . $this->anneeProduction .')';
    }

    public function definirNombreRoue($nbRoue)
    {

        if(round ($nbRoue/2)==$nbRoue /2){
        $this ->nbRoue =$nbRoue;
    }


}
}

?>