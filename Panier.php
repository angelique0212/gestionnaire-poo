
<?php

class Panier {
    public $fruits =[];

    function ajouterFruit($fruit){
        $this->fruits[] = $fruit;
    }

    function calculerPoids(){
        $p = 0;
        foreach ($this->fruits as $fruit) {
            $p += $fruit->getPoids();
            }
        return $p;
        }
    }

